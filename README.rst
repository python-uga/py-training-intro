Python training UGA 2017
========================

**A training to acquire strong basis in Python to use it efficiently**

The code of this training has been written for Python 3.

To display and/or modify the presentations, use the Makefile. See::

  make help

Repositories
------------

The old repository is https://sourcesup.renater.fr/projects/py-training-uga/

We now use a repository hosted in the Gitlab of UGA:
https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga

- Clone the repository with Git::

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-intro.git

- Clone the repository with Mercurial (and the extension hg-git, as explained
  `here
  <http://fluiddyn.readthedocs.io/en/latest/mercurial_bitbucket.html>`_)::

    hg clone git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-intro.git


Authors
-------

Pierre Augier (LEGI), Cyrille Bonamy (LEGI), Eric Maldonado (Irstea), Franck
Thollard (ISTERRE), Christophe Picard (LJK)

