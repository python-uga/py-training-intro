#!/usr/bin/env python3
"""Computes basic statistics on file that contains a set of lines, each line
containing one float.

"""

file_name = '../data/file0.1.txt'

sum = 0.0
number = 0

with open(file_name) as handle:
    for line in handle:
        elem = float(line)
        sum = sum + elem
        number += 1
handle.close()

# not formatted output
# print('nb={}, sum={}, avg={}'.format(number, sum, sum/float(number)))

# formatted output
print('file = "{}"\nnb = {}; sum = {:.2f}; avg = {:.2f}'.format(
    file_name, number, sum, sum/float(number)))
