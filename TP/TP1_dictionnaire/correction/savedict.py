#!/usr/bin/env python3

""" save a dictionnary either as a pickle file or as a json file """

import json
import pickle

def json_save_dict(dictionnary, outfile):
    """ save the dictionnary in outfile in json format
    :param dictionnary: the dictionnary to save
    :type dictionnary: dict
    :param outfile: the out file name
    :type outfile: str
    """
    with open(outfile, "w") as _handle:
        json.dump(dictionnary, _handle)
    _handle.close()

def pickle_save_dict(dictionnary, outfile):
    """ save the dictionnary in outfile in pickle format
    :param dictionnary: the dictionnary to save
    :type dictionnary: dict
    :param outfile: the out file name
    :type outfile: str
    """
    with open(outfile, 'wb') as _handle:
        pickle.dump(dictionnary, _handle)
    _handle.close()

