#!/usr/bin/env python3
"""
Load content of a wiki log file, extract the meaningful column
and save it to pickle file.
"""

import sys
import time
import argparse
import savedict
import wikipedialogs

if __name__ == "__main__":
    # no argument given -> we add -h so that the help will be triggered
    if len(sys.argv) == 1:
        sys.argv.append('-h')

    parser = argparse.ArgumentParser(description=("Parse a wikipedia log and prints"
                                                   " the stats in a file"))
    parser.add_argument("-i", type=str, help="the input log file to parse")
    parser.add_argument("-f", type=str, default='j', choices=['p','j'],
                        help="the format to output (pickle or json)")
    parser.add_argument("-p", type=str, default='r', choices=['r','s'], 
                        help="choose the parser (regex or str manipulations)")
    parser.add_argument("-o", type=str, help="the name of the output file")
    args = parser.parse_args()

    # opening the file
    stats = {}
    try:
        with open(args.i, "r")  as _handle:
            if args.p == "r":
                start_time = time.time()
                stats = wikipedialogs.stats_wikipedia_log_re(_handle)
                end_time = time.time()
            else:
                start_time = time.time()
                stats = wikipedialogs.stats_wikipedia_log_str(_handle)
                end_time = time.time()
        print("parsing {} in {:.2f} secs".format(args.i, end_time-start_time))
    except Exception as excpt:
        print("fail to parse in file {}: {}".format(args.i, str(excpt)))
        sys.exit(1)
    try:
        if args.f == "j":
            savedict.json_save_dict(stats, args.o)
        else:
            savedict.pickle_save_dict(stats, args.o)
    except Exception as excpt:
        print("fail to save result {}: {}".format(args.o, str(excpt)))
        sys.exit(2)

