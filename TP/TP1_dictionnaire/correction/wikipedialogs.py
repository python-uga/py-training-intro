#!/usr/bin/env python3
""" parse wikipedia query log either using index stuff either using regex"""

import re

def sort_wiki_files(directory, starter):
    """Read directory directory and return a list
       of wiki files sorted according to the timestamps
    :directory: a directory to read.
    :returns: a sorted list of string
    """
    dic = {}
    fic = ""
    for fic in os.listdir(directory):
        abs_name = os.path.join(directory, fic)
        if os.path.isfile(abs_name) and fic.startswith(starter):
            line = ""
            with open(abs_name, "r") as _handle:
                line = _handle.readline()
                dic[fic] = float(line.split()[1])
    return [y[0] for y in sorted(dic.items(), key=lambda x:x[1])]

def stats_wikipedia_log_re(infile):
    """
    stats will be a dictionnary such that key is a country, or "upload"
    and stats["en"] represents the number of time the url referes to en
    using regex
    """
    stats = {}
    regex = re.compile("https?://([^\.]+).wik")
    for line in infile:
        m = regex.search(line)
        if m:
            url = m.groups()[0]
            try:
                stats[url] += 1
            except Exception as e:
                stats[url] = 0
        """if m:
            url = m.groups()[0]
            if url in stats:
                stats[url] += 1
            else:
                stats[url] = 1
        """
    return stats

def stats_wikipedia_log_str(infile):
    """
    stats will be a dictionnary such that key is a country, or "upload"
    and stats["en"] represents the number of time the url referes to en
    """
    stats = {}
    for line in infile:
        elems = line.split()
        url = elems[2]
        # skiping http://
        pos = 0
        if "http://" in url:
            pos = url.index("http://") + len("http://")
        elif "https://" in url:
            pos = url.index("https://") + len("https://")
        else:
            continue
        url = url[pos:]
        # searching for .wiki
        pos = 0
        if ".wik" in url:
            pos = url.index(".wik")
            url = url[:pos]
        else:
            continue
        if url in stats:
            stats[url] += 1
        else:
            stats[url] = 1
    return stats


