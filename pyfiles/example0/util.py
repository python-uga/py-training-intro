print('begin of util.py')
a = 1
b = 2

def print_ab():
    print('in function print_ab: a = {}; b = {}'.format(a, b))

print('in util.py, __name__ =', __name__)
if __name__ == '__main__':
    print_ab()
    print('end of util.py')
